import math
from Crypto.Util.number import inverse

class ECC():
    # E_prime(a, b)
    # y ^ 2 mod prime = (x ^ 3 + ax + b) mod prime
    def __init__(self, prime, a, b):
        self.m = prime
        self.a = a
        self.b = b
        self._y_coord = None
        self._ymap = None
        self.y_coord

    def calc_lambda(self, p, q, verbose = True):
        m = self.m
        if p[0] == q[0] and p[1] == q[1]:
            kA = self.a
            lA = (3 * (p[0] ** 2) + kA)
            lB = 2 * p[1]
            if verbose:
                print(f'lambda: (3 * ({p[0]} ** 2) + {kA}) / (2 * {p[1]}) mod {m} =', end=' ')
        else:
            lA = p[1] - q[1]
            lB = p[0] - q[0]
            if verbose:
                print(f'lambda: {lA}/{lB} mod {m} =', end=' ')

            fpb = math.gcd(lA, lB)
            lA //= fpb
            lB //= fpb
            if lA < 0 and lB < 0:
                lA *= -1
                lB *= -1

        lamd = lA * inverse(lB, m) % m
        if verbose:
            print(f"{lA}/{lB} mod {m} = {lamd}")
        return lamd

    def add(self, p, q, verbose = True):
        if verbose:
            print(f'p: {p}')
            print(f'q: {q}')

        m = self.m
        if p[0] == q[0] and p[1] != q[1]:
            if verbose:
                print("Koordinat-x sama -> r = o\n")
            return (0, 0)
        if p == (0, 0):
            if verbose:
                print("o + q = q\n")
            return q
        elif q == (0, 0):
            if verbose:
                print("p + o = p\n")
            return p
        
        lamd = self.calc_lambda(p, q, verbose = verbose)

        xR = (lamd ** 2 - p[0] - q[0]) % m
        if verbose:
            print(f"Xr: ({lamd} ** 2 - {p[0]} - {q[0]}) mod {m} = {(lamd ** 2 - p[0] - q[0])} mod {m} = {xR}")

        yR = (lamd * (p[0] - xR) - p[1]) % m
        if verbose:
            print(f"Yr: ({lamd} * ({p[0]} - {xR}) - {p[1]}) mod {m} = {(lamd * (p[0] - xR) - p[1])} mod {m} = {yR}")
            print('r:', (xR, yR), end='\n\n')
        return (xR, yR)

    def get_ordo(self, p, kMax = 1000, verbose = True):
        if p[0] == p[1] and p[1] == 0: 
            return 1
        q = p
        for i in range(2, kMax+1):
            q = self.add(p, q, verbose = verbose)
            if verbose:
                print(f'{i}P = {q}')
            if q[0] == q[1] and q[1] == 0:
                if verbose:
                    print('Ordo:', i)
                return i

    def point_from_x(self, x, verbose = True):
        c = x ** 3 + self.a * x + self.b
        c_mod = c % self.m
        
        y_valid = self._ymap[c_mod]
        point_ret = [(x, y) for y in y_valid]
        if verbose:
            print(f'x: {x}')
            print(f'({x} ** 3 + {self.a} * {x} + {self.b}) mod {self.m} = {c} mod {self.m} = {c_mod}')
            print(f'Point: {point_ret}\n')
        return point_ret

    def verbose_x(self):
        for x in range(self.m):
            self.point_from_x(x, True)
    
    def verbose_y(self):
        for y in range(self.m):
            print(f'y: {y}')
            print(f'{y} ** 2 mod {self.m} = {y ** 2} mod {self.m} = {y ** 2 % self.m}\n')
        
    @property
    def points(self):
        ret = []
        for x in range(self.m):
            p = self.point_from_x(x, False)
            ret += p
        return ret           

    @property
    def y_coord(self):
        if self._y_coord == None:
            ret = [(i * i) % self.m for i in range(self.m)]
            tmp_map = [[] for _ in range(self.m)]
            for i in range(self.m):
                c = ret[i]
                tmp_map[c].append(i)
            self._y_coord = ret
            self._ymap = tmp_map
        return self._y_coord
    
    @property
    def ymap(self):
        if self._ymap == None:
            self.y_coord
        return self._ymap


ecc = ECC(13, 2, -3)
points = ecc.points

ecc.verbose_y()
ecc.verbose_x()
ordo = ecc.get_ordo((9, 9))