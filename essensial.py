#
# Polynomial Representation GF(2)
#
def bin_to_poly(n):
    suku = []
    deg = len(bin(int(n, 2))[2:])
    n_10 = int(n, 2)
    for i in range(deg, 0, -1):
        if (n_10 & (1 << (i - 1))):
            if i == 1:
                suku.append('1')
            else:
                suku.append(f'x^{i - 1}')
    return ' + '.join(suku)

def dec_to_poly(n):
    return bin_to_poly(bin(n))

#
# MODULO OPERATION IN POLYNOMIAL GF(2)
#
# If f(x) = x^6 + x^4 + x^2 + 1, then f = '101001'
# If m(x) = x^3 + x, then m = '1010'
# Remove all leading zero
def bin_poly_mod(f, m):
    deg_f = len(bin(int(f, 2))[2:])
    deg_m = len(bin(int(m, 2))[2:])
    
    f_10 = int(f, 2)
    m_10 = int(m, 2)
    pembagi = m_10 << (deg_f - deg_m)

    r = f_10
    for i in range(deg_f, deg_m - 1, -1):
        if (r & (1 << (i - 1))):
            r ^= pembagi
        pembagi >>= 1 
    return bin(r)[2:]

def dec_poly_mod(f, m):
    return int(bin_poly_mod(bin(f), bin(m)))

#
# MULTIPLICATION OPERATION IN POLYNOMIAL GF(2)
#
# If f(x) = x^6 + x^4 + x^2 + 1, then f = '101001'
# If g(x) = x^3 + x, then g = '1010'
# Remove all leading zero
def bin_poly_multi(f, g, m = None):
    res = 0
    f_10 = int(f, 2)
    g_10 = int(g, 2)
    for i in range(63):
        if (g_10 & (1 << i)):
            res ^= f_10 << i        
    ret = bin(res)[2:]
    if m != None:
        ret = bin_poly_mod(ret, m)
    return ret

def dec_poly_multi(f, g, m = None):
    ret = bin_poly_multi(bin(f), bin(g))
    if m != None:
        ret = bin_poly_mod(ret, bin(m))
    return int(ret)

g = bin_poly_multi('1010111', '10000011')
g = bin_poly_mod(g, '100011011')
print(bin_to_poly(g))

r = bin_poly_mod('0001110011', '1010')
print(bin_to_poly(r))

z = dec_poly_mod(115, 10)
print(dec_to_poly(z))